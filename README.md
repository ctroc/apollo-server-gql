# APOLLO SERVER GRAPHQL

## Deploy localhost:

#### Clone this repo and go inside to root folder, run this comands:

build image:

```bash
docker-compose build
```
up your image

```bash
docker-compose up
```

## Use app like any other node apps:

#### Go inside apollo-server-app folder.

Install packages:

```bash
npm install
```

Run app with nodemon:

```bash
npm run start
```

#### Dont forget configure your envs var in .env file (redisBaseUrl, ApiBaseUrls)
