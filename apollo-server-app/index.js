import { ApolloServer, gql } from "apollo-server";
import { RedisCache } from "apollo-server-cache-redis";
import dotenv from 'dotenv';

/* Get env vars, redis urls */
dotenv.config();
const { redisBaseUrl, redisPort } = process.env;

/* Here get yours class and functions from controllers */
import  {
  TreasuryAPI,
  TreasuryApiResolver,
  TreasuryApiSchema
} from "./src/controllers/ApiTreasuryController";
/* Kpis */
import {
  FinancingAPI,
  LoanAPI,
  KpisSchema,
  KpisResolver
} from "./src/controllers/KpisController";


/* Dont remove next line (all typeDef extend from this) */
const typeDef = gql`type Query`;

/* Instance Apollo Server */
const server = new ApolloServer({
  typeDefs: [  /* Here subscribe yours schemas */
    typeDef,
    TreasuryApiSchema,
    KpisSchema,
  ],
  resolvers: [ /* Here subscribe yours resolvers mehtods */
    TreasuryApiResolver,
    KpisResolver,
  ],
  cache: new RedisCache({
    host: redisBaseUrl,
    port: redisPort,
  }),
  dataSources: () => ({ /* Here subscribe yours dataSources */
   TreasuryAPI: new TreasuryAPI(),
   FinancingAPI: new FinancingAPI(),
   LoanAPI: new LoanAPI(),
 })
});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
