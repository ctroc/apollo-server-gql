import { gql } from "apollo-server";

const typeDef = gql`
  type Financing {
    millions_invested: Int
    financed_credits: Int
  }
  type Loan {
    annual_average_return: Float
    unpaid_capital: Float
    annual_average_interest: Float
  }
  type AllKpis {
    financing: Financing
    loan: Loan
  }
  extend type Query {
    Kpis: AllKpis
  }
`;

export default typeDef;
