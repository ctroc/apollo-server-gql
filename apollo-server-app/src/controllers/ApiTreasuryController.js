import TreasuryAPI from "../models/ApiTreasuryModel";
import resolvers  from "../resolvers/ApiTreasuryResolver";
import typeDef from "../typeDefs/ApiTreasuryTypeDef";

export  {
  TreasuryAPI,
  resolvers as TreasuryApiResolver,
  typeDef as TreasuryApiSchema
};
