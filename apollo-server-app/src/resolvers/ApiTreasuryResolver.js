//import 'regenerator-runtime/runtime';
const resolvers = {
  Query: {
    Enterprises: async (_source, _args, { dataSources }) => {
      return dataSources.TreasuryAPI.getEnterprises();
    },
  },
};

export default resolvers;
