import { RESTDataSource } from "apollo-datasource-rest";
import dotenv from 'dotenv';

/* Get env vars, base urls */
dotenv.config();
const { loanApiBaseUrl } = process.env;

class LoanAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = loanApiBaseUrl;
  }

  async getKpisLoan() {
    return this.get(`loans/kpi/`, null, {cacheOptions: {ttl: 300}});
  }
}

export default LoanAPI;
